import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService, API_BANK, API_CRUD } from '../app.service';

@Injectable({
  providedIn: 'root'
})
export class NewAddresseeService extends AppService {

  constructor(http: HttpClient) { 
    super(http);
  }

  putAddressee(data: any): Observable<any> {
    this.endpoint = API_CRUD+'/addressee/save';
    return this.put(this.endpoint, null, data);
  }

  getAddresseeList(): Observable<any> {
    this.endpoint = API_CRUD+'/addressee/list';
    return this.get(this.endpoint);
  }

}
