import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService, API_BANK, API_CRUD } from '../app.service';

@Injectable({
  providedIn: 'root'
})
export class BankService extends AppService {

  constructor(http: HttpClient) { 
    super(http);
  }

  getBank(): Observable<any> {
    this.endpoint = API_BANK;
    return this.get(this.endpoint);
  }
  getTransferHistory(): Observable<any> {
    this.endpoint = API_CRUD+'/payments/list';
    return this.get(this.endpoint);
  }

  getAccountType(): Observable<any> {
    this.endpoint = API_CRUD+'/payments/account-type';
    return this.get(this.endpoint);
  }

  postTransfer(data: any): Observable<any> {
    this.endpoint = API_CRUD+'/payments/transfer';
    return this.post(this.endpoint, null, data);
  }

}
