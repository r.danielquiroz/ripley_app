import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

export const API_BANK = environment.API_BANK;
export const API_CRUD = environment.API_CRUD;


@Injectable({
  providedIn: 'root'
})
export class AppService {
  public endpoint = '/';
  constructor(private http: HttpClient) {
  }

  generateHeader(): HttpHeaders {
    const header = new HttpHeaders({ 'Content-Type': 'application/json' });
    return header;
  }

  get(url: string, type: any = 'json'): Observable<any> {
    return this.http.get(url, { headers: this.generateHeader(), responseType: type });
  }

  put(url: string, type: any = 'json', object: any = null): Observable<any> {
    return this.http.put(url, object, { headers: this.generateHeader(), responseType: type });
  }

  post(url: string, type: any = 'json', data: any = null): Observable<any> {
    return this.http.post(url, data, { headers: this.generateHeader(), responseType: type });
  }
}
