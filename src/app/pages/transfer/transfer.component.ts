import { Component, OnInit } from '@angular/core';
import { NewAddresseeService } from '../../services/newAddressee.service';
import Swal from 'sweetalert2'
import { BankService } from 'src/app/services/bank.service';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.sass']
})
export class TransferComponent implements OnInit {
  public addressee: any;
  public selcetAddressee: any;
  public amount: number;
  constructor(
    private newAddresseeService: NewAddresseeService,
    private banckService: BankService,
  ) {
    this.selcetAddressee = null;
    this.amount = 0;
  }

  ngOnInit(): void {
    this.getAddresseeList();
  }


  getAddresseeList() {
    this.newAddresseeService.getAddresseeList()
      .subscribe((res) => {
        this.addressee = res;
      });
  }

  info(data: any) {
    this.selcetAddressee = this.addressee.filter((a: any) => +a.id === +data.target.value)[0];
  }

  sendTransfer() {
    if (this.amount < 1) {
      Swal.fire({
        icon: 'error',
        title: 'Lo sentimos...',
        text: 'Las transferencias deben ser mayores a 0',
      });
    } else {
      const data = {
        id: this.selcetAddressee.id,
        email: this.selcetAddressee.email,
        amount: this.amount,
        date: new Date(),
        user: this.selcetAddressee
      }

      this.banckService.postTransfer(data)
        .subscribe((res) => {
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Transferencia realizada correctamente!',
            showConfirmButton: false,
            timer: 1500
          })
        }, error => {
          Swal.fire({
            icon: 'error',
            title: 'Lo sentimos...',
            text: 'Tenemos algunos probemas, favor intente mas tarde ',
          });
        });
    }
  }
}
