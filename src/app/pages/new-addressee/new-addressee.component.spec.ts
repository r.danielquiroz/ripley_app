import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewAddresseeComponent } from './new-addressee.component';

describe('NewAddresseeComponent', () => {
  let component: NewAddresseeComponent;
  let fixture: ComponentFixture<NewAddresseeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewAddresseeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewAddresseeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
