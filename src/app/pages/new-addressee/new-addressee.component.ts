import { Component, OnInit } from '@angular/core';
import { BankService } from '../../services/bank.service';
import { NewAddresseeService } from '../../services/newAddressee.service';
import Swal from 'sweetalert2'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-new-addressee',
  templateUrl: './new-addressee.component.html',
  styleUrls: ['./new-addressee.component.sass']
})
export class NewAddresseeComponent implements OnInit {

  public banks: any;
  public accountType: any;
  public newAddressee: FormGroup;
  public validRut: boolean;

  constructor(
    private banckService: BankService,
    private newAddresseeService: NewAddresseeService,
    private fb: FormBuilder
  ) {
    this.newAddressee = this.fb.group({
      name: [null, [Validators.required, Validators.pattern("([a-zA-Z',.-]+( [a-zA-Z',.-]+)*){2,30}")]],
      rut: [null, Validators.required],
      email: [null, [Validators.required, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')]],
      phone: [null, [Validators.required, Validators.pattern('[6789][0-9]{7}')]],
      bank: [null, Validators.required],
      accountType: [null, Validators.required],
      accountNumber: [null, [Validators.required, Validators.pattern('[0-9]{7,15}')]],
    });
    this.validRut = false;
  }

  ngOnInit(): void {
    this.getBank();
    this.getAccountType();
  }

  getBank() {
    this.banckService.getBank()
      .subscribe((res) => {
        this.banks = res.banks;
      }, error => {
        Swal.fire({
          icon: 'error',
          title: 'Lo sentimos...',
          text: 'tenemos algunos probemas, favor intente mas tarde',
        });
      });
  }

  getAccountType() {
    this.banckService.getAccountType()
      .subscribe((res) => {
        this.accountType = res;
      }, error => {
        Swal.fire({
          icon: 'error',
          title: 'Lo sentimos...',
          text: 'tenemos algunos probemas, favor intente mas tarde',
        });
      });
  }

  submit(form: any) {
    form = form.value;
    this.newAddresseeService.putAddressee(form)
      .subscribe((res) => {
        console.log(res);
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Nuevo destinatario guardado!',
          showConfirmButton: false,
          timer: 1500
        })
      }, error => {
        Swal.fire({
          icon: 'error',
          title: 'Lo sentimos...',
          text: 'tenemos algunos probemas, favor intente mas tarde',
        });
      });
  }

  formatCliente(cliente: any) {
    if (cliente.value && cliente.value !== '') {
      cliente.value = cliente.value.replace(/[.-]/g, '')
        .replace(/^(\d{1,2})(\d{3})(\d{3})(\w{1})$/, '$1.$2.$3-$4')
      this.newAddressee.controls.rut.setValue(cliente.value);
      var Fn = {
        // Valida el rut con su cadena completa "XXXXXXXX-X"
        validaRut: function (rutCompleto: any) {
          rutCompleto = cliente.value = cliente.value.replace(/[.-]/g, '')
            .replace(/^(\d{1,2})(\d{3})(\d{3})(\w{1})$/, '$1$2$3-$4')

          if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test(rutCompleto))
            return false;
          var tmp = rutCompleto.split('-');
          var digv = tmp[1];
          var rut = tmp[0];
          if (digv == 'K') digv = 'k';

          return (Fn.dv(rut) == digv);
        },
        dv: function (T: any) {
          var M = 0, S = 1;
          for (; T; T = Math.floor(T / 10))
            S = (S + T % 10 * (9 - M++ % 6)) % 11;
          return S ? S - 1 : 'k';
        }
      }
      this.validRut = Fn.validaRut(cliente.value);
    }
  }

  validFormat(data: string): boolean {
    let valid = false;
    switch (data) {
      case 'name':
        valid = (this.newAddressee.controls.name.value && (this.newAddressee.controls.name.value === '' || !this.newAddressee.controls.name.valid)) ? true : false;
        break;
      case 'email':
        valid = (this.newAddressee.controls.email.value && (this.newAddressee.controls.email.value === '' || !this.newAddressee.controls.email.valid)) ? true : false;
        break;
      case 'phone':
        valid = (this.newAddressee.controls.phone.value && (this.newAddressee.controls.phone.value === '' || !this.newAddressee.controls.phone.valid)) ? true : false;
        break;
      case 'accNumber':
        valid = (this.newAddressee.controls.accountNumber.value && (this.newAddressee.controls.accountNumber.value === '' || !this.newAddressee.controls.accountNumber.valid)) ? true : false;
        break;
      case 'rut':
        valid = (this.newAddressee.controls.rut.value && (this.newAddressee.controls.rut.value === '' || !this.validRut)) ? true : false;
        break;

      default:
        break;
    }
    return valid;
  }

}
