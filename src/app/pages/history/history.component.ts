import { Component, OnInit } from '@angular/core';
import { BankService } from 'src/app/services/bank.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.sass']
})
export class HistoryComponent implements OnInit {

  public list: any;

  constructor(
    private banckService: BankService,
  ) {
    this.list = [];
  }

  ngOnInit(): void {
    this.getTransferHistory();
  }

  getTransferHistory() {
    this.banckService.getTransferHistory()
      .subscribe((res) => {
        this.list = res;
      });
  }
}
