import { Component, OnInit } from '@angular/core';
import { faUser, faCreditCard, faHistory } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements OnInit {
  Iuser = faUser;
  Itransfer = faCreditCard;
  Ihistory = faHistory;
  constructor() { }

  ngOnInit(): void {
  }

}
