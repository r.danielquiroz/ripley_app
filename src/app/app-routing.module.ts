import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewAddresseeComponent } from './pages/new-addressee/new-addressee.component';
import { TransferComponent } from './pages/transfer/transfer.component';
import { HistoryComponent } from './pages/history/history.component';
const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/new-addresse' },
  { path: 'new-addresse', component: NewAddresseeComponent },
  { path:'transfer', component: TransferComponent },
  { path: 'history', component: HistoryComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
