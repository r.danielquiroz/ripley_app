export const environment = {
  production: true,
  API_BANK: 'https://bast.dev/api/banks.php',
  API_CRUD: 'https://ripley-crud-production.herokuapp.com',
};
